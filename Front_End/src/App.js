import "./App.css";
import DeliveryCharges from "./components/DeliveryCharges";
import Placed_Delivered from "./components/PlacedDelivered";
import DataFetching from "./components/DataFetching";
import Shipped_Delivered from "./components/ShippedDelivered";
import SingleMultiple from "./components/SingleMultiple";
import WhatsappWebstore from "./components/WhatsappWebstore";

function App() {
  return (
    <div className="App">
      <div>
        <div>
          <Placed_Delivered />
        </div>
        <div>
          <Shipped_Delivered />
        </div>
      </div>
      <div>
        <div>
          <DeliveryCharges />
        </div>
        <div>
          <WhatsappWebstore />
        </div>
      </div>
      <div>
        <div>
          <SingleMultiple />
        </div>
        <div>
          <DataFetching />
        </div>
      </div>
    </div>
  );
}

export default App;
