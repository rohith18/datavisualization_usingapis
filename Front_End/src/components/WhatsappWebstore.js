import { useEffect, useState } from "react";
import { PieChart, Pie, Tooltip } from "recharts";

const WhatsappWebstore = () => {
  const [item1, setItem1] = useState(0);
  const [item2, setItem2] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("http://127.0.0.1:5000/");
      const data = await response.json();
      setItem1((item) => data.whatsapp_orders);
      setItem2((item) => data.webstore_orders);
    };
    fetchData();
  }, []);

  let data = [item1, item2];
  let total = data[0] + data[1];
  let percentages = [
    parseFloat(((data[0] / total) * 100).toFixed(1)),
    parseFloat(((data[1] / total) * 100).toFixed(1)),
  ];

  const data01 = [
    { name: "WhatsApp Orders", value: percentages[0] },
    { name: "WebStore Orders", value: percentages[1] },
  ];

  return (
    <div className="display-card">
      <h2>1P Orders : Orders on WhatsApp and WebStore</h2>
      <p>( hover over a specific slice to know more details )</p>
      <PieChart width={300} height={300}>
        <Pie
          data={data01}
          dataKey="value"
          cx={150}
          cy={150}
          outerRadius={80}
          label
        />
        <Tooltip />
      </PieChart>
    </div>
  );
};

export default WhatsappWebstore;
