import { useEffect, useState } from "react";
import { PieChart, Pie, Tooltip } from "recharts";

const PlacedDelivered = () => {
  const [item1, setItem1] = useState(0);
  const [item2, setItem2] = useState(0);
  const [item3, setItem3] = useState(0);
  const [item4, setItem4] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("http://127.0.0.1:5000/");
      const data = await response.json();
      setItem1((item) => data.less_than_30_min);
      setItem2((item) => data.to_1hr);
      setItem3((item) => data.to_2hr);
      setItem4((item) => data.to_6hr);
    };
    fetchData();
  }, []);

  let data = [item1, item2, item3, item4];
  let total = 494;
  let percentages = [
    ((data[0] / total) * 100).toFixed(1),
    ((data[1] / total) * 100).toFixed(1),
    ((data[2] / total) * 100).toFixed(1),
    ((data[3] / total) * 100).toFixed(1),
  ];

  const data01 = [
    { name: "<= 30min", value: parseFloat(percentages[0]) },
    { name: "30min - 1hr", value: parseFloat(percentages[1]) },
    { name: "1hr - 2hr", value: parseFloat(percentages[2]) },
    { name: "2hr - 6hr", value: parseFloat(percentages[3]) },
  ];

  return (
    <div className="display-card">
      <h2>1P Orders : Total time taken</h2>
      <p>( hover over a specific slice to know more details )</p>
      <PieChart width={300} height={300}>
        <Pie
          data={data01}
          dataKey="value"
          cx={150}
          cy={150}
          outerRadius={80}
          label
        />
        <Tooltip />
      </PieChart>
    </div>
  );
};

export default PlacedDelivered;
