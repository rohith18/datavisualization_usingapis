import { useEffect, useState } from "react";
import { PieChart, Pie, Tooltip } from "recharts";

const SingleMultiple = () => {
  const [item1, setItem1] = useState(0);
  const [item2, setItem2] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("http://127.0.0.1:5000/");
      const data = await response.json();
      setItem1((item) => data.single_item);
      setItem2((item) => data.multiple_items);
    };
    fetchData();
  }, []);

  let data = [item1, item2];
  let total = data[0] + data[1];
  let percentages = [
    ((data[0] / total) * 100).toFixed(1),
    ((data[1] / total) * 100).toFixed(1),
  ];

  const data01 = [
    { name: "Single Order", value: parseFloat(percentages[0]) },
    { name: "Multiple Orders", value: parseFloat(percentages[1]) },
  ];

  return (
    <div className="display-card">
      <h2>1P Orders : SingleOrder and MultipleOrders on WhatsApp</h2>
      <p>( hover over a specific slice to know more details )</p>
      <PieChart width={300} height={300}>
        <Pie
          data={data01}
          dataKey="value"
          cx={150}
          cy={150}
          outerRadius={80}
          label
        />
        <Tooltip />
      </PieChart>
    </div>
  );
};

export default SingleMultiple;
