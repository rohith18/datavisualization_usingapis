import { useEffect, useState } from "react";
import { PieChart, Pie, Tooltip } from "recharts";

const DeliveryCharges = () => {
  const [item1, setItem1] = useState(0);
  const [item2, setItem2] = useState(0);
  const [item3, setItem3] = useState(0);

  let data = [item1, item2, item3];
  let total = data[0] + data[1] + data[2];
  let percentages = [
    ((data[0] / total) * 100).toFixed(1),
    ((data[1] / total) * 100).toFixed(1),
    ((data[2] / total) * 100).toFixed(1),
  ];

  const data01 = [
    { name: "<= 25 Rs", value: parseFloat(percentages[0]) },
    { name: "25 Rs - 50 Rs", value: parseFloat(percentages[1]) },
    { name: "> 50 Rs", value: parseFloat(percentages[2]) },
  ];

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("http://127.0.0.1:5000/");
      const data = await response.json();
      setItem1((item) => data.less_than_25rs);
      setItem2((item) => data.to_50rs);
      setItem3((item) => data.greater_than_50rs);
    };
    fetchData();
  }, []);

  return (
    <div className="display-card">
      <h2>1P Orders : Delivery Charges (by Dunzo)</h2>
      <p>( hover over a specific slice to know more details )</p>
      <PieChart width={300} height={300}>
        <Pie
          data={data01}
          dataKey="value"
          cx={150}
          cy={150}
          outerRadius={80}
          label
        />
        <Tooltip />
      </PieChart>
    </div>
  );
};

export default DeliveryCharges;
