import json
import matplotlib.pyplot as plt

data = []
whatsapp_orders = 0
webstore_orders = 0
total = 0

with open("./data.json") as json_file:
  data = json.load(json_file)

for item in data:
  if (item.get("type")!="pickup" and item.get("status")=="delivered" and item.get("dunzoId")!=None):
    if len(item.get("items")) == 0 :
      whatsapp_orders = whatsapp_orders + 1
      total = total +1
    if len(item.get("items")) !=0  :
      webstore_orders = webstore_orders + 1
      total = total +1


def whatsapp_webstore_test():
    mode = [whatsapp_orders, webstore_orders]
    labels = ["WhatsApp Orders", "WebStore Orders"]
    plt.pie(mode, labels = labels, autopct = "%2.1f%%")
    plt.title("1P Orders\nWhatsApp and WebStore", bbox={'facecolor':'0.8', 'pad':5})
    plt.show()

if __name__ == "__main__":
    whatsapp_webstore_test()
    
