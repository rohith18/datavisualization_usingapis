import json
from flask import Flask, request
from flask_cors import CORS
from placed_delivered_test import less_than_30_min, to_1hr, to_2hr, to_6hr
from shipped_delivered_test import less_than_10min, to_20min, to_40min, greater_than_40min
from delivery_charge_test import less_than_25rs, to_50rs, greater_than_50rs
from whatsapp_webstore_test import whatsapp_orders, webstore_orders
from single_multiple_test import single_item, multiple_items
from delivered_cancelled_test import delivered_items, cancelled_items 

app = Flask(__name__)
CORS(app)

@app.route("/")
def index():
    response_object = { 
        "less_than_30_min":less_than_30_min, "to_1hr":to_1hr, "to_2hr":to_2hr, "to_6hr":to_6hr,
        "less_than_10min":less_than_10min, "to_20min":to_20min, "to_40min":to_40min, "greater_than_40min":greater_than_40min,
        "less_than_25rs":less_than_25rs, "to_50rs":to_50rs, "greater_than_50rs":greater_than_50rs,
        "whatsapp_orders":whatsapp_orders, "webstore_orders":webstore_orders,
        "single_item":single_item, "multiple_items":multiple_items,
        "delivered_items":delivered_items, "cancelled_items":cancelled_items,
        "message":"nothing part 2"
     }
    response_object = json.dumps(response_object)
    return response_object

if __name__ == "__main__":
    app.run()


