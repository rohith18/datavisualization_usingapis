import json
import matplotlib.pyplot as plt

data = None
delivery_charge = 0
less_than_25rs = 0
to_50rs = 0
greater_than_50rs = 0
delivery_charges = []
total = 0

with open("./data.json") as json_file:
  data = json.load(json_file)

for item in data:
  if (item.get("type")!="pickup" and item.get("status")=="delivered" and item.get("dunzoId")!=None and item.get("dunzoId")!="" and item.get("deliveryCharge")!=None ):
        delivery_charge = item.get("deliveryCharge")
        if delivery_charge <= 25 :
            less_than_25rs = less_than_25rs + 1
            total = total + 1
        if (delivery_charge > 25 and delivery_charge <=50 ) :
            to_50rs = to_50rs + 1
            total = total + 1
        if delivery_charge > 50 :
            greater_than_50rs = greater_than_50rs + 1
            total = total + 1

def delivery_charge():
    delivery_charges = [less_than_25rs, to_50rs, greater_than_50rs]
    labels = ["<= 25 Rs", "26 Rs - 50 Rs", "51 Rs - 70 Rs"]
    plt.pie(delivery_charges, labels = labels, autopct = "%2.1f%%")
    plt.title("1P Orders\nDelivery Charges (by Dunzo)", bbox={'facecolor':'0.8', 'pad':5})
    plt.show()

if __name__ == "__main__":
    delivery_charge()








