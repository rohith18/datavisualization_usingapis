import json
import matplotlib.pyplot as plt

data = None
shipped_at = 0
delivered_at = 0
time_took = 0
less_than_10min = 0
to_20min = 0
to_40min = 0
greater_than_40min = 0
total = 0
time_period = []

with open("./data.json") as json_file:
  data = json.load(json_file)

for item in data:
  if (item.get("type")!="pickup" and item.get("status")=="delivered" and item.get("dunzoId")!=None and item.get("dunzoId")!=""):
    for i in item.get("timeline"):
        if i.get("action") == "shipped":
          shipped_at = i.get("epoch")
        if i.get("action") == "delivered":
          delivered_at = i.get("epoch")
    if (shipped_at!=None and delivered_at!=None):
      time_took = delivered_at - shipped_at
      if time_took <= 600:
        less_than_10min = less_than_10min + 1
      if (time_took >600 and time_took <= 1200):
        to_20min = to_20min + 1
        total = total + 1
      if (time_took > 1200 and time_took <=2400):
        to_40min = to_40min + 1
        total = total + 1
      if time_took > 2400:
        greater_than_40min = greater_than_40min + 1
        total = total + 1

def shipped_delivered_test():
    time_period = [less_than_10min, to_20min, to_40min, greater_than_40min]
    labels = ["<= 10min", "10min - 20min", "20min - 40min", " > 40min"]
    plt.pie(time_period, labels = labels, autopct = "%2.1f%%")
    plt.title("1P Orders\nTime taken for delivery (by Dunzo)", bbox={'facecolor':'0.8', 'pad':5})
    plt.show()

if __name__ == "__main__":
    shipped_delivered_test()

