import json
import matplotlib.pyplot as plt

data = []
single_item = 0
multiple_items = 0
total = 0

with open("./data.json") as json_file:
  data = json.load(json_file)

for item in data:
  if (item.get("type")!="pickup" and item.get("status")=="delivered" and item.get("dunzoId")!=None and item.get("dunzoId")!=""):
    if len(item.get("items")) == 0 :
      if item.get("texts")!= None:
        if len(item.get("texts")) == 1:
          single_item = single_item + 1
          total = total + 1
        if len(item.get("texts")) > 1:
          multiple_items = multiple_items + 1
          total = total + 1

def single_multiple_test():
    mode = [single_item, multiple_items]
    labels = ["Single Item", "Multiple Items"]
    plt.pie(mode, labels = labels, autopct = "%2.1f%%")
    plt.title("1P Orders\nSingle Item and Multiple Items in WhatsApp Orders", bbox={'facecolor':'0.8', 'pad':5})
    plt.show()

if __name__ == "__main__":
    single_multiple_test()

    
