import json
import matplotlib.pyplot as plt

data = None
placed_at = 0
delivered_at = 0
time_took = 0
less_than_30_min = 0
to_1hr = 0
to_2hr = 0
to_6hr = 0
total = 0
time_period = []

with open("./data.json") as json_file:
  data = json.load(json_file)

for item in data:
  if (item.get("type")!="pickup" and item.get("status")=="delivered" and item.get("dunzoId")!=None and item.get("dunzoId")!=""):
    for i in item.get("timeline"):
        if i.get("action") == "placed":
          placed_at = i.get("epoch")
        if i.get("action") == "delivered":
          delivered_at = i.get("epoch")
    if (placed_at!=None and delivered_at!=None):
      time_took = delivered_at - placed_at
      if time_took <= 1800:
        less_than_30_min = less_than_30_min + 1
        total = total + 1
      if (time_took > 1800 and time_took <=3600):
        to_1hr = to_1hr + 1
        total = total + 1
      if (time_took > 3600 and time_took <=7200):
        to_2hr = to_2hr + 1
        total = total + 1
      if (time_took > 7200 and time_took <=21600):
        to_6hr = to_6hr + 1
        total = total + 1

def placed_delivered_test():
    time_period = [less_than_30_min, to_1hr, to_2hr, to_6hr]
    labels = ["<= 30min", "30min - 1hr", "1hr - 2hr", "2hr - 6hr"]
    plt.pie(time_period, labels = labels, autopct = "%2.1f%%")
    plt.title("1P Orders\nTotal Time taken", bbox={'facecolor':'0.8', 'pad':5})
    plt.show()

if __name__ == "__main__":
    placed_delivered_test()








